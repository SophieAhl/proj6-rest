<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of laptops</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $laptops = $obj->Laptops;
            foreach ($laptops as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>listAll?</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
            $what = $obj->what;
            echo "<li>$what</li>";
            foreach ($what as $w) {
                echo "<li>$w</li>";
            }
            ?>
        </ul>
    </body>
</html>
