# Laptop Service

from flask import Flask
from flask_restful import Resource, Api

import os
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': [
            	'Mac OS', 
            	'Dell', 
            	'Windozzee',
	    		'Yet another laptop!',
	    		'Yet yet another laptop!'
            ]
        }

class listAll(Resource):
    def get(self):
        i = {'km':1}
        items = [{'km':str(item)} for item in db.tododb.find()]
        app.logger.debug(str(items))
        return {
            'what':items
        }

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/')
api.add_resource(listAll, '/listAll')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
